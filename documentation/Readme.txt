#
# Copyright (C) 2010 ST-Ericsson AS
# Author: Erwan Bracq / erwan.bracq@stericsson.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
#

Modem Init Deamon

================


I) DBUS specification

Service         com.stericsson.mid
Interface       com.stericsson.mid.Modem
Object path     /com/stericsson/mid/Modem


## Methods
                string Reboot()
                       Reboot request of the modem. Return OK if request
                       accepted. REJECTED otherwise. A Reboot() request can be
		       issued at any time. And it will be executed as soon as
		       modem reach "on" state.
		       A Reboot() request can not be cancelled.
                string PowerOn()
		       Modem power on request. Return OK if request accepted.
		       REJECTED otherwise.
		       Only possible when modem is in "off" state.
		string PowerOff()
		       Modem power off request. Return OK if request accepted.
		       REJECTED otherwise.
		       A PowerOff() request can be issued at any time (except
		       when modem is already in "off" state). And will be
		       executed as soon as modem reach "on" state.
		       A PowerOff() request can not be cancelled.
		string UploadImage()
                       Transfer firmware upgrade image to modem. Return OK if
                       transfer was successful, or an error as specified
                       in the below chapter on firmware upgrade return values.
                       Note that the DBus reply msg is sent after the transfer
                       completes.
                string Upgrade()
                       Initiate a firmware upgrade of the modem. Return OK if
                       upgrade was successful, or an error as specified
                       in the below chapter on firmware upgrade return values.
                       Note: the DBus reply msg is sent after the upgrade
                       completes.
                string GetUpgradeStatus()
                       Check the modem firmware upgrade status. Returns the
                       modem upgrade state, or an error as specified
                       in the below chapter on firmware upgrade return values.
                string GetState()
                       Return the current state of the modem.
                string GetWarn()
                       Return the current modem warning.
                string GetModemId()
                       Return modem version.


## Signals
                StateChange(string status)
                       This signal indicates a state change.
                ModemWarn(string warning)
                       This signal indicates a modem warning.
                FWUpgradeStatus(string phase)
                       This signal indicates a started firmware upgrade
                       activity.
                FWImgXferProgress(int percent)
                       This signal will be sent during a firmware upgrade image
                       transfer to the modem and indicates the transfer progress
                       in percent.


## Properties
                string State [readonly]
                       The modems state sent from Modem Init Daemon when a
                       modem state change occurs.

                       "unknown"     - Used only for MID startup sequence for
                                       initialisation. This state is not
                                       broadcasted.
                       "booting"     - Modem is powered up (flashed version)
                                       or
                                       Modem is powered up and firmware upload
                                       is completed. (flashless version)
                       "upgrading"   - Firmware upgrade ongoing
                                       or
                                       Flashing manager under execution - modem in
                                       service mode.
                       "on"          - Modem has booted and is ready for use.
                                       This implies that all AT channels are
                                       available, the modem might be in e.g.
                                       flight mode.
                       "dumping"     - Modem has crashed and dump is ongoing.
                       "prepare_off" - Modem will be powered off in t + 5 seconds. All AT client should take
                                       appropriate action to disconnect from AT channel.
                       "off"         - Modem is powered off.


                string Warning [readonly]
                       The warning message sent from Modem Init Daemon when a
                       modem warning occurs

                       "high_temperature" - Modem high temperature warning. A modem shutdown is initiated.
                       "low_voltage"      - Battery low voltage warning.
                       "cut_voltage"      - Battery cut voltage notification. A modem shutdown is initiated.
                       "none"             - No warning. This value is only used for
                                            GetWarn method when no warn is ongoing.


                string ModemId [readonly]
                       The modem version Id.

II) MID service commands



III) Firmware upgrade

## Firmware Upgrade Method Return Values

The following list contains the possible return types from the DBus methods
related to firmware upgrade. The return types are strings, and everthing except
OK indicates an error.

        General return types:
                       OK
                       SYSTEM_ERROR
                       INVALID_REQ_PHASE
                       REQ_IN_PROGRESS
                       UNSUPP_MODEM
                       MODEM_SHUTDOWN
                       AT_TIMEOUT
                       REQ_REJECTED
                       AT_CHN_ERROR
                       AT_CMD_ERROR
                       INVALID_MODEM_STATE
                       FILESYS_ERROR
                       IMG_SIZE_ERROR
                       LOW_BATTERY
                       NO_FW_IMG

        Firmware image transfer specific:
                       MODEM_FS_INSUFF_SPACE
                       MODEM_FS_WRITE_ERROR
                       PKT_TIMEOUT
                       FRAME_TIMEOUT
                       WRONG_FILE_NAME
                       WRONG_PKG_SIZE
                       WRONG_OBJ_SIZE
                       WRONG_PKT_DATA
                       WRONG_PKT_IDX
                       MODEM_UNDEF_ERROR


## Firmware Upgrade Modem State

Calling the GetUpgradeStatus() DBus method will return the modem firmware
upgrade state, or possibly one or the above error messages.
The fw status can be one of the following values.

NOTE: This list may be shortened since the final modem implementation will
      probably not support all these values.

             * FIRMWARE_DELIVERY_PACKAGE_STATE_NOT_INSTALLED
             * FIRMWARE_DELIVERY_PACKAGE_STATE_INITIATING
             * FIRMWARE_DELIVERY_PACKAGE_STATE_INITIATED
             * FIRMWARE_DELIVERY_PACKAGE_STATE_APPLYING
             * FIRMWARE_DELIVERY_PACKAGE_STATE_APPLIED
             * FIRMWARE_DELIVERY_PACKAGE_STATE_PENDING_FINALIZATION
             * FIRMWARE_DELIVERY_PACKAGE_STATE_INSTALLED
             * FIRMWARE_DELIVERY_PACKAGE_STATE_PENDING_ROLLBACK
             * FIRMWARE_DELIVERY_PACKAGE_STATE_PENDING_CLEANUP
             * FIRMWARE_DELIVERY_PACKAGE_STATE_PENDING_RESTART



IV) Internal state machine


IV.a) Poweron sequence

IV.b) Poweroff sequence

IV.c) Flash sequence

IV.d) Firmware upload sequence

IV.e) Firmware upgrade sequence

IV.f) Crash dump sequence

V) Configuration file



