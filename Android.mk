#
# Copyright (C) 2010 ST-Ericsson AS
# Author: Erwan Bracq / erwan.bracq@stericsson.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
#
LOCAL_PATH := $(call my-dir)
STE_MID_PATH := $(LOCAL_PATH)

include $(CLEAR_VARS)
ifneq (,$(filter $(BOARD_MODEM),STE_M570 STE_M730))
LOCAL_MODULE := ste_conf
$(LOCAL_MODULE)_TARGET_CPP := $(TARGET_TOOLS_PREFIX)cpp$(HOST_EXECUTABLE_SUFFIX)
$(LOCAL_MODULE)_MID_CONF := $(TARGET_OUT_ETC)/mid.conf
$(LOCAL_MODULE)_DEFINES := -DMOTOROLA_FEATURE

ifneq (,$(filter $(TARGET_BUILD_VARIANT),user))
$(LOCAL_MODULE)_DEFINES += -DUSER_BUILD
endif
ifneq (,$(filter $(TARGET_BUILD_VARIANT),userdebug))
$(LOCAL_MODULE)_DEFINES += -DUSERDEBUG_BUILD
endif
ifneq (,$(filter $(TARGET_BUILD_VARIANT),eng))
$(LOCAL_MODULE)_DEFINES += -DENG_BUILD
endif
ifneq (,$(filter $(BOARD_MODEM),STE_M570))
$(LOCAL_MODULE)_DEFINES += -DSTE_M570
ifneq (,$(filter $(HW_CONFIG),p0_edison p1_edison))
$(LOCAL_MODULE)_DEFINES += -DP0_EDISON -DP1_EDISON
else
$(LOCAL_MODULE)_DEFINES += -DP2_EDISON
endif
endif
ifneq (,$(filter $(BOARD_MODEM),STE_M730))
$(LOCAL_MODULE)_DEFINES += -DSTE_M730
endif

$($(LOCAL_MODULE)_MID_CONF): $(LOCAL_PATH)/etc/Flashed_mid.conf
	@mkdir -p $(dir $@)
	@$(ste_conf_TARGET_CPP) -x assembler-with-cpp -nostdinc -P -undef $(ste_conf_DEFINES) $< $@

PRODUCT_COPY_FILES += $($(LOCAL_MODULE)_MID_CONF)
else
LOCAL_PATH := $(STE_MID_PATH)/etc
LOCAL_MODULE := Flashed_mid.conf
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/system/etc
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_PATH := $(STE_MID_PATH)/etc
LOCAL_MODULE := Flashless_mid.conf
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/system/etc
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)
endif

# Shared libusb build
include $(CLEAR_VARS)
LOCAL_PATH:= $(STE_MID_PATH)
LOCAL_SRC_FILES := \
    external/libusb-1.0.8/libusb/core.c \
    external/libusb-1.0.8/libusb/descriptor.c \
    external/libusb-1.0.8/libusb/io.c \
    external/libusb-1.0.8/libusb/sync.c \
    external/libusb-1.0.8/libusb/os/linux_usbfs.c

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/external/libusb-1.0.8/ \
    $(LOCAL_PATH)/external/libusb-1.0.8/libusb/ \
    $(LOCAL_PATH)/external/libusb-1.0.8/libusb/os \
    $(LOCAL_PATH)/external/config/libusb

LOCAL_MODULE := libusb
LOCAL_MODULE_TAGS := optional

LOCAL_CFLAGS := -Wno-pointer-arith -Wno-sign-compare -Wno-empty-body -Wno-type-limits -fPIC -D'TIMESPEC_TO_TIMEVAL(tv, ts)=do{(tv)->tv_sec=(ts)->tv_sec;(tv)->tv_usec=(ts)->tv_nsec/1000;}while(0)'

LOCAL_PRELINK_MODULE := false

include $(BUILD_SHARED_LIBRARY)

# Shared libconfig build
include $(CLEAR_VARS)
LOCAL_PATH:= $(STE_MID_PATH)
LOCAL_SRC_FILES := \
    external/libconfig-1.4.6/lib/grammar.c \
    external/libconfig-1.4.6/lib/libconfig.c \
    external/libconfig-1.4.6/lib/strbuf.c \
    external/libconfig-1.4.6/lib/scanctx.c \
    external/libconfig-1.4.6/lib/scanner.c

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/external/libconfig-1.4.6/lib

LOCAL_MODULE := libconfig
LOCAL_MODULE_TAGS := optional

LOCAL_PRELINK_MODULE := false

include $(BUILD_SHARED_LIBRARY)

# Shared libcap build
include $(CLEAR_VARS)
LOCAL_PATH:= $(STE_MID_PATH)
LOCAL_SRC_FILES := \
	external/libcap-2.21/libcap/cap_alloc.c \
	external/libcap-2.21/libcap/cap_extint.c \
	external/libcap-2.21/libcap/cap_flag.c \
	external/libcap-2.21/libcap/cap_proc.c \
	external/libcap-2.21/libcap/cap_text.c

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/external/libcap-2.21/libcap/include \
    $(LOCAL_PATH)/external/libcap-2.21-generated

LOCAL_MODULE := libcap
LOCAL_MODULE_TAGS := optional

LOCAL_PRELINK_MODULE := false

include $(BUILD_SHARED_LIBRARY)

# Shared Apache Portable Runtime build
include $(CLEAR_VARS)
LOCAL_PATH:= $(STE_MID_PATH)

LOCAL_SRC_FILES := \
    external/apr-1.4.2/mmap/unix/common.c \
    external/apr-1.4.2/mmap/unix/mmap.c \
    external/apr-1.4.2/threadproc/unix/threadpriv.c \
    external/apr-1.4.2/threadproc/unix/procsup.c \
    external/apr-1.4.2/threadproc/unix/proc.c \
    external/apr-1.4.2/threadproc/unix/signals.c \
    external/apr-1.4.2/threadproc/unix/thread.c \
    external/apr-1.4.2/strings/apr_snprintf.c \
    external/apr-1.4.2/strings/apr_strings.c \
    external/apr-1.4.2/strings/apr_cpystrn.c \
    external/apr-1.4.2/strings/apr_strnatcmp.c \
    external/apr-1.4.2/strings/apr_fnmatch.c \
    external/apr-1.4.2/strings/apr_strtok.c \
    external/apr-1.4.2/build/jlibtool.c \
    external/apr-1.4.2/dso/unix/dso.c \
    external/apr-1.4.2/memory/unix/apr_pools.c \
    external/apr-1.4.2/time/unix/time.c \
    external/apr-1.4.2/time/unix/timestr.c \
    external/apr-1.4.2/shmem/unix/shm.c \
    external/apr-1.4.2/user/unix/userinfo.c \
    external/apr-1.4.2/user/unix/groupinfo.c \
    external/apr-1.4.2/support/unix/waitio.c \
    external/apr-1.4.2/tables/apr_hash.c \
    external/apr-1.4.2/tables/apr_tables.c \
    external/apr-1.4.2/locks/unix/thread_mutex.c \
    external/apr-1.4.2/locks/unix/global_mutex.c \
    external/apr-1.4.2/locks/unix/thread_cond.c \
    external/apr-1.4.2/locks/unix/proc_mutex.c \
    external/apr-1.4.2/random/unix/sha2_glue.c \
    external/apr-1.4.2/random/unix/apr_random.c \
    external/apr-1.4.2/random/unix/sha2.c \
    external/apr-1.4.2/file_io/unix/copy.c \
    external/apr-1.4.2/file_io/unix/filestat.c \
    external/apr-1.4.2/file_io/unix/filepath_util.c \
    external/apr-1.4.2/file_io/unix/filepath.c \
    external/apr-1.4.2/file_io/unix/open.c \
    external/apr-1.4.2/file_io/unix/dir.c \
    external/apr-1.4.2/file_io/unix/seek.c \
    external/apr-1.4.2/file_io/unix/fileacc.c \
    external/apr-1.4.2/file_io/unix/mktemp.c \
    external/apr-1.4.2/file_io/unix/readwrite.c \
    external/apr-1.4.2/file_io/unix/filedup.c \
    external/apr-1.4.2/file_io/unix/flock.c \
    external/apr-1.4.2/file_io/unix/buffer.c \
    external/apr-1.4.2/file_io/unix/fullrw.c \
    external/apr-1.4.2/file_io/unix/pipe.c \
    external/apr-1.4.2/file_io/unix/tempdir.c \
    external/apr-1.4.2/misc/unix/env.c \
    external/apr-1.4.2/misc/unix/getopt.c \
    external/apr-1.4.2/misc/unix/start.c \
    external/apr-1.4.2/misc/unix/otherchild.c \
    external/apr-1.4.2/misc/unix/rand.c \
    external/apr-1.4.2/misc/unix/version.c \
    external/apr-1.4.2/misc/unix/charset.c \
    external/apr-1.4.2/misc/unix/errorcodes.c \
    external/apr-1.4.2/network_io/unix/sockets.c \
    external/apr-1.4.2/network_io/unix/socket_util.c \
    external/apr-1.4.2/network_io/unix/sockaddr.c \
    external/apr-1.4.2/network_io/unix/multicast.c \
    external/apr-1.4.2/network_io/unix/inet_pton.c \
    external/apr-1.4.2/network_io/unix/sendrecv.c \
    external/apr-1.4.2/network_io/unix/sockopt.c \
    external/apr-1.4.2/network_io/unix/inet_ntop.c \
    external/apr-1.4.2/poll/unix/pollset.c \
    external/apr-1.4.2/poll/unix/port.c \
    external/apr-1.4.2/poll/unix/select.c \
    external/apr-1.4.2/poll/unix/epoll.c \
    external/apr-1.4.2/poll/unix/pollcb.c \
    external/apr-1.4.2/poll/unix/kqueue.c \
    external/apr-1.4.2/poll/unix/poll.c \
    external/apr-1.4.2/passwd/apr_getpass.c \
    external/apr-1.4.2/atomic/unix/ppc.c \
    external/apr-1.4.2/atomic/unix/s390.c \
    external/apr-1.4.2/atomic/unix/ia32.c \
    external/apr-1.4.2/atomic/unix/solaris.c \
    external/apr-1.4.2/atomic/unix/builtins.c \
    external/apr-1.4.2/atomic/unix/mutex.c \

    #external/apr-1.4.2/build/aplibtool.c
    #external/apr-1.4.2/locks/unix/thread_rwlock.c

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/external/apr-1.4.2-generated \
    $(LOCAL_PATH)/external/apr-1.4.2/include \
    $(LOCAL_PATH)/external/apr-1.4.2/include/arch/unix

LOCAL_MODULE := libapr-1
LOCAL_MODULE_TAGS := optional

LOCAL_SHARED_LIBRARIES := libdl
LOCAL_PRELINK_MODULE := false

LOCAL_CFLAGS=-Wno-sign-compare -Wno-missing-field-initializers

include $(BUILD_SHARED_LIBRARY)

# Static libmid build
include $(CLEAR_VARS)
LOCAL_PATH:= $(STE_MID_PATH)
LOCAL_SRC_FILES := \
    src/core/mid_gpio_noinline.c \
    src/core/mid_gpio.c \
    src/core/mid_power.c \
    src/core/mid_ipc_dbus.c \
    src/core/mid_flash.c \
    src/core/mid_conf.c \
    src/core/mid_caif.c \
    src/core/mid_caif_transport_linux.c \
    src/core/mid_exec.c \
    src/core/mid_mfa.c \
    src/core/mid_seq.c

LOCAL_C_INCLUDES := \
    $(call include-path-for, dbus) \
    $(LOCAL_PATH)/include/core \
    $(TOP)/bionic/libc/kernel/common/linux/caif \
    $(LOCAL_PATH)/external/libconfig-1.4.6/lib \
    $(LOCAL_PATH)/external/apr-1.4.2-generated \
    $(LOCAL_PATH)/external/apr-1.4.2/include \
    $(LOCAL_PATH)/external/libat \
    $(LOCAL_PATH)/external/libcap-2.21/libcap/include \
    $(LOCAL_PATH)/external/libcap-2.21-generated

LOCAL_SHARED_LIBRARIES := libcutils libutils libat libapr libcap

LOCAL_MODULE := libmid
LOCAL_MODULE_TAGS := optional

LOCAL_CFLAGS := -DDEBUG_TRACE -W -Wextra -Wall -fPIC -D_GNU_SOURCE
ifneq (,$(filter $(BOARD_MODEM),STE_M570 STE_M730))
LOCAL_CFLAGS += -DMOTOROLA_FEATURE
endif

include $(BUILD_STATIC_LIBRARY)

# Shared libmoliU33x build
include $(CLEAR_VARS)
LOCAL_PATH:= $(STE_MID_PATH)

LOCAL_MODULE_NAME := libmoliU33x.so
LOCAL_MODULE_TAGS := optional

LOCAL_MODULE_CLASS := SHARED_LIBRARY

LOCAL_PREBUILT_LIBS:= \
    proprietary/$(LOCAL_MODULE_NAME)

include $(BUILD_MULTI_PREBUILT)

# Shared libmoliU5xx build
include $(CLEAR_VARS)
LOCAL_PATH:= $(STE_MID_PATH)

LOCAL_MODULE_NAME := libmoliU5xx.so
LOCAL_MODULE_TAGS := optional

LOCAL_MODULE_CLASS := SHARED_LIBRARY

LOCAL_PREBUILT_LIBS:= \
    proprietary/$(LOCAL_MODULE_NAME)

include $(BUILD_MULTI_PREBUILT)

# TODO base this lib on u300-libparser
include $(CLEAR_VARS)
LOCAL_PATH:= $(STE_MID_PATH)
LOCAL_SRC_FILES:= \
        external/libat/atchannel.c \
	external/libat/misc.c \
        external/libat/at_tok.c

LOCAL_SHARED_LIBRARIES := libcutils

# Disable prelink, or add to build/core/prelink-linux-arm.map
LOCAL_PRELINK_MODULE := false

LOCAL_MODULE := libat
LOCAL_MODULE_TAGS := optional

ifneq ($(findstring STE_,$(BOARD_MODEM)),)
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
intermediates := $(local-intermediates-dir)/libat

GEN := $(addprefix $(intermediates)/,atchannel.h misc.h at_tok.h)
$(GEN): $(intermediates)/%.h : $(LOCAL_PATH)/external/libat/%.h
	@mkdir -p $(dir $@)
	@cat $< > $@
LOCAL_GENERATED_SOURCES += $(GEN)
endif

include $(BUILD_SHARED_LIBRARY)

# Program build
include $(CLEAR_VARS)
LOCAL_PATH:= $(STE_MID_PATH)
LOCAL_SRC_FILES := \
    src/core/mid_dev.c \
    src/core/mid_statemachine.c \
    src/core/mid.c

LOCAL_C_INCLUDES := \
    $(call include-path-for, dbus) \
    $(TOP)/bionic/libc/kernel/common/linux/caif \
    $(LOCAL_PATH)/include/core \
    $(LOCAL_PATH)/external/libusb-1.0.8/libusb \
    $(LOCAL_PATH)/external/libconfig-1.4.6/lib \
    $(LOCAL_PATH)/external/apr-1.4.2-generated \
    $(LOCAL_PATH)/external/apr-1.4.2/include/ \
    $(LOCAL_PATH)/external/libat \
    $(LOCAL_PATH)/external/libcap-2.21/libcap/include \
    $(LOCAL_PATH)/external/libcap-2.21-generated

ifneq ($(USE_MID_LOCAL_INCLUDE),)
    LOCAL_C_INCLUDES += $(LOCAL_PATH)/external/include
endif

LOCAL_CFLAGS := -DDEBUG_TRACE -DMID_PARSE_KER_CMDLINE -W -Wextra -Wall -fPIC -g -rdynamic -D_GNU_SOURCE
ifneq ($(findstring STE_,$(BOARD_MODEM)),)
LOCAL_CFLAGS += -DMOTOROLA_FEATURE
ifneq (,$(filter $(BOARD_MODEM),STE_M570))
LOCAL_CFLAGS += -DSTE_M570
endif
ifneq (,$(filter $(BOARD_MODEM),STE_M730))
LOCAL_CFLAGS += -DSTE_M730
endif
endif

LOCAL_STATIC_LIBRARIES := libmid

LOCAL_SHARED_LIBRARIES := libapr-1 libcutils libutils libdbus libusb libconfig libat libdl libcap

LOCAL_MODULE := mid
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

# Lifecycle Logger
include $(CLEAR_VARS)
LOCAL_PATH:= $(STE_MID_PATH)
LOCAL_SRC_FILES := \
    src/lifecycle/lifecycle_logger.c \

LOCAL_C_INCLUDES := \
    $(call include-path-for, dbus) \

LOCAL_CFLAGS := -W -Wextra -Wall -fPIC -g -rdynamic -D_GNU_SOURCE

LOCAL_STATIC_LIBRARIES :=

LOCAL_SHARED_LIBRARIES := libdbus

LOCAL_MODULE := lifecycle
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

