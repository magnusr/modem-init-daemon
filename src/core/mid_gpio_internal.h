/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Magnus Reftel / magnus.xm.reftel@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#include <mid_gpio.h>

#include <apr_file_io.h>

#include <limits.h>

struct mid_gpio_context {
	int gpio_ctrl;
	int gpio_no_aclow;
	int gpio_no_xport;
	int gpio_no_dir;
	char gpio_base_path[PATH_MAX];
	struct gpio mid_gpios_tab[GPIO_ID_COUNT];
	apr_file_t* value_fds[GPIO_ID_COUNT];
	apr_file_t* irqwake_fds[GPIO_ID_COUNT];
	apr_file_t* unexport_fd;
	apr_pool_t* pool;
};
