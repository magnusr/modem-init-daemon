/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Dmitry Tarnyagin / dmitry.tarnyagin@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#include <string.h>
#include <ctype.h>
#include <errno.h>
#include "mid_seq.h"

void seq_triml(struct seq_t *seq)
{
	for (; seq->seq < seq->end; ++seq->seq) {
		if (!isspace(*seq->seq))
			break;
	}
}

void seq_trimr(struct seq_t *seq)
{
	for (; seq->seq < seq->end; --seq->end) {
		if (!isspace(seq->end[-1]))
			break;
	}
}

void seq_tok(struct seq_t *l, struct seq_t *r,
	     const char *tok)
{
	r->end = l->end;
	for (r->seq = l->seq; r->seq != r->end; ++r->seq) {
		if (strchr(tok, *r->seq)) {
			l->end = r->seq++;
			break;
		}
	}
}

int seq_copyto(const struct seq_t *seq, char *buf, size_t buf_size)
{
	if (seq_len(seq) >= buf_size)
		return -EINVAL;
	memcpy(buf, seq_str(seq), seq_len(seq));
	buf[seq_len(seq)] = 0;
	return 0;
}
