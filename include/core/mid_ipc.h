/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_IPC_H
#define MID_IPC_H

#include <apr_poll.h>

#include "mid.h"

enum ipc_msg_type {
    NULLMSG,
    STATECHANGE,
    MODEMWARN,
    FW_UPGR_STATUS,
    FW_UPGR_XFER_PROGRESS,
#ifdef MOTOROLA_FEATURE
    MFARESULT,
#endif /* MOTOROLA_FEATURE */
};

struct ipc_msg_statechange {
    enum ipc_msg_type type;
    char *newstate;
    char* reason;
};

struct ipc_msg_modemwarn {
    enum ipc_msg_type type;
    char *warnmsg;
};

struct ipc_msg_fw_upgr_status {
    enum ipc_msg_type type;
    char *statusmsg;
};

struct ipc_msg_fw_upgr_xfer_progress {
    enum ipc_msg_type type;
    int percent;
};

#ifdef MOTOROLA_FEATURE
struct ipc_msg_mfaresult {
    enum ipc_msg_type type;
    char *result;
};
#endif /* MOTOROLA_FEATURE */

union ipc_msg {
    enum ipc_msg_type type;
    struct ipc_msg_statechange msg_statechange;
    struct ipc_msg_modemwarn msg_modemwarn;
    struct ipc_msg_fw_upgr_status msg_fw_upgr_status;
    struct ipc_msg_fw_upgr_xfer_progress msg_fw_xfer_progress;
#ifdef MOTOROLA_FEATURE
    struct ipc_msg_mfaresult msg_mfaresult;
#endif /* MOTOROLA_FEATURE */
};

#ifdef MOTOROLA_FEATURE
/* Connect to the IPC medium: e.g. dbus */
int ipc_medium_connect(struct mid *midinfo,int reconnect);
/* Disconnect from IPC medium: Ignores Errors */
int ipc_medium_cleanup(void);
#else
int ipc_medium_connect(struct mid *midinfo);
#endif
/* Disconnect from the IPC medium: e.g. dbus */
int ipc_medium_disconnect(void);
/* Prepare message */
int ipc_prepare_message(union ipc_msg *msg, enum ipc_msg_type type, ...);
/* Send message */
int ipc_send_message(union ipc_msg msg);
/* Processe IPC event */
void ipc_notify_for_event(int index, short event);
void ipc_process_event();

size_t ipc_get_num_fds();
size_t ipc_get_fds(apr_pollfd_t* out, size_t max);

/**
 * Sends a delayed IPC response back to caller.
 * @msg: Double pointer to message from IPC module (e.g. fw_upgr_reply_msg)
 * @status: Text string with the reply message
 *
 * The msg ptr must point to an IPC message already configured by the IPC
 * module, so that it can simply be sent off with no further configuration.
 *
 * The msg pointer is set to NULL by the function since the IPC module takes
 * ownership of the actual msg when it is sent.
 */
int ipc_send_async_reply(void **msg, const char *status);

#endif /* ifndef MID_IPC_H */
