/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_GPIO_DEF_H
#define MID_GPIO_DEF_H

#include "mid_settings.h"

/* Kernel 2.6.32- does not support gpio poll operation. A specific driver
 * needs to be present to provide this functionality to MID
 */
/* #define	BEFORE_KERNEL_2_6_33 */

/* String maximum lengh including end of string - value translated to int */
#define GPIO_NAME_SZ		11
/* Allows only 0 or 1, including end of string */
#define GPIO_VALUE_SZ		2
/* Maximum sysfs entry path, including end of string */
#define GPIO_SYSFS_SZ		257

/* GPIO define, should be specified outside MID */
#ifndef GPIO_MOD_ON
#define GPIO_MOD_ON			-1
#endif
#ifndef GPIO_MOD_RESET
#define GPIO_MOD_RESET			-1
#endif
#ifndef GPIO_MOD_RESOUT2
#define GPIO_MOD_RESOUT2		-1
#endif
#ifndef GPIO_MOD_SERVICE
#define GPIO_MOD_SERVICE		-1
#endif
#ifndef GPIO_MOD_PWRRSTIN
#define GPIO_MOD_PWRRSTIN		-1
#endif
#ifndef GPIO_MOD_PWRRSTOUT
#define GPIO_MOD_PWRRSTOUT		-1
#endif
/* If not specified outside MID, GPIO active high */
#ifndef GPIO_MOD_ON_POL
#define GPIO_MOD_ON_POL			0
#endif
#ifndef GPIO_MOD_RESET_POL
#define GPIO_MOD_RESET_POL		0
#endif
#ifndef GPIO_MOD_RESOUT2_POL
#define GPIO_MOD_RESOUT2_POL		0
#endif
#ifndef GPIO_MOD_SERVICE_POL
#define GPIO_MOD_SERVICE_POL		0
#endif
#ifndef GPIO_MOD_PWRRSTIN_POL
#define GPIO_MOD_PWRRSTIN_POL		0
#endif
#ifndef GPIO_MOD_PWRRSTOUT_POL
#define GPIO_MOD_PWRRSTOUT_POL		0
#endif
/* If not specified outside MID, OUT GPIO are low
 * and IN GPIO int edge is NONE
 */
#ifndef GPIO_MOD_ON_INIT_ST
#define GPIO_MOD_ON_INIT_ST		0
#endif
#ifndef GPIO_MOD_RESET_INIT_ST
#define GPIO_MOD_RESET_INIT_ST		0
#endif
#ifndef GPIO_MOD_RESOUT2_INIT_ST
#define GPIO_MOD_RESOUT2_INIT_ST	0
#endif
#ifndef GPIO_MOD_SERVICE_INIT_ST
#define GPIO_MOD_SERVICE_INIT_ST	0
#endif
#ifndef GPIO_MOD_PWRRSTIN_INIT_ST
#define GPIO_MOD_PWRRSTIN_INIT_ST	0
#endif
#ifndef GPIO_MOD_PWRRSTOUT_INIT_ST
#define GPIO_MOD_PWRRSTOUT_INIT_ST	0
#endif

/* Standard kernel GPIO SYSFS entries */
#define GPIO_EXPORT		"%s/export"
#define GPIO_UNEXPORT		"%s/unexport"
#define GPIO_VALUE		"%s/gpio%d/value"
#define GPIO_DIRECTION		"%s/gpio%d/direction"
#ifndef BEFORE_KERNEL_2_6_33
#define GPIO_EDGE		"%s/gpio%d/edge"
#define GPIO_ACTIVE_LOW		"%s/gpio%d/active_low"
#define GPIO_IRQ_WAKE		"%s/gpio%d/irq_wake"
#endif
/* Temporary waiting kernel 2.6.33+ for GPIO poll() */
#ifdef 	BEFORE_KERNEL_2_6_33
#define GPIO_RESOUT2_VAL_POLL	"%s/modem_rstout2_gpio%d"
#define GPIO_PWRRSTIN_VAL_POLL	"%s/modem_pwrstin_gpio%d"
#else
#define GPIO_RESOUT2_VAL_POLL	GPIO_VALUE
#define GPIO_PWRRSTIN_VAL_POLL	GPIO_VALUE
#endif
#define GPIO_N_ASSERT		0
#define GPIO_N_DEASSERT		1
#define GPIO_ASSERT		1
#define GPIO_DEASSERT		0

#endif /* ifndef MID_GPIO_DEF_H */
