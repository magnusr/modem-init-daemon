/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_GPIO_H
#define MID_GPIO_H

#include <stdbool.h>

#include <apr_file_io.h>

#include "mid_gpio_def.h"

enum gpio_direction {
	GPIO_DIR_IN = 0,
	GPIO_DIR_OUT,
	GPIO_DIR_OUT_LOW,
	GPIO_DIR_OUT_HIGH
};

enum gpio_edge {
	NONE = 0,
	RISING,
	FALLING,
	BOTH
};

enum gpio_active_low {
	ACTIVE_HIGH = 0,
	ACTIVE_LOW
};

enum {
	INACTIVE = 0,
	ACTIVE
};

enum gpio_physical {GPIO_PHYSICAL_INVALID=-1};

struct gpio {
	enum gpio_physical gpio;
	int value;
	int direction;
	int active_low;
	int gpio_ctrl_enable;
	int gpio_ctrl_noaclow;
};

enum gpio_id
{
	GPIO_ID_ON,
	GPIO_ID_RESET,
	GPIO_ID_RESOUT2,
	GPIO_ID_SERVICE,
	GPIO_ID_PWRRSTIN,
	GPIO_ID_PWRRSTOUT,

	GPIO_ID_LOWEST=GPIO_ID_ON,
	GPIO_ID_HIGHEST=GPIO_ID_PWRRSTOUT,
	GPIO_ID_COUNT=GPIO_ID_HIGHEST+1
};

struct mid_gpio_context;

#ifndef MID_GPIO_USED_INLINE
bool mid_gpio_used(struct mid_gpio_context* context, enum gpio_id gpio);
#endif

struct mid_gpio_context* mid_gpio_init(const struct gpio mid_gpios[], const char *gpio_base_path, int gpio_ctrl_enable, int gpio_no_aclow, int gpio_no_xport, int gpio_no_dir);
void mid_gpio_deinit(struct mid_gpio_context* context);

void mid_gpio_release(struct mid_gpio_context* context);
int mid_gpio_assert(struct mid_gpio_context* context, enum gpio_id gpio);
int mid_gpio_deassert(struct mid_gpio_context* context, enum gpio_id gpio);
int mid_gpio_get_value(struct mid_gpio_context* context, enum gpio_id gpio);

void mid_gpio_release_all(struct mid_gpio_context* context);

apr_file_t* mid_gpio_get_fd(struct mid_gpio_context* context, enum gpio_id gpio);
bool mid_gpio_is_ctrl_enabled(struct mid_gpio_context* context);

#endif /* ifndef MID_GPIO_H */
