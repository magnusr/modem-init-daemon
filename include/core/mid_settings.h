/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_SETTINGS_H
#define MID_SETTINGS_H

#include "mid_log.h"

/* Software version */
#define MODEM_ARCH_STRING		"OSMIUM"
#define MODEM_BUILD_STRING		"UNKNOWN"

/* Dynamic librairies */
#define MID_MOLIU33x_NAME		"libmoliU33x.so"
#define MID_MOLIU5xx_NAME		"libmoliU5xx.so"
#define MID_MOLIU7xx_NAME		"libmoliU7xx.so"

/* MID default settings, overwritten by configuration file and / or command line arguments */
/* TODO not all settings are overwritten now. Work ongoing */

/* Daemon options */
#define RUNNING_DIR			"/lib/firmware/ste/modem"
#define MID_PID_FILE			"/var/run/mid.pid"
#define MID_LOG_FILE			"/var/log/mid.log"
#define MID_CFG_FILE			"/etc/mid.conf"
#define CMD_PARM_NAME			"mid_cfg_file="
#define DUMP_DEFAULT_FILENAME           "dump.txt"
#define DUMP_HEADER_DEFAULT_FILENAME    "small_dump.txt"
#define MID_MAX_DUMP_FILES		4
#define MID_MAX_BOOT_ATTEMPT		3
#define MID_MAX_FLASH_ATTEMPT		3
#define MID_SHUTDOWN_RETRIES		0
#define MID_DBG_DEFAULT_LEVEL		MID_LEVEL_MAX
#define MID_ENABLE_LINKSWITCH		0
#define MID_POLL_LAT			1 /* 1 second poll timeout */

/* Link setup */
#define PRI_DEFAULT_PATH		"/dev/ttyS0"
#define SEC_DEFAULT_PATH		"/dev/ttyS0"
/* Default settings for UART */
#define UART_CFG 			(CLOCAL | CREAD)
#define UART_VTIME			10

/* IPC setup */
#define MID_IPC_OPS_DELAY		500000 /* 500 milliseconds delay */
#define DBUS_CONNECTION_NAME		"com.stericsson.mid"
#define DBUS_OBJECT_PATH		"/com/stericsson/mid"
#define DBUS_OBJECT_INTERFACE		"com.stericsson.mid.Modem"
#define DBUS_MAX_WATCHERS		2 /* 2 should be enough - 1 reader / 1 writer */

/* AT settings */
#define MID_AT_CHN_TIMEOUT		60 /* 1 min timeout by default on the MID AT channel */

/* GPIOS setup */
#define MID_GPIO_CTRL			0  /* no gpio control */
#define MODEM_POWER_ON_LAT		2  /* 2 second delay */
#define MODEM_POWER_OFF_LAT		15 /* 15 second delay */
#define MID_GPIO_FORCE_OFF		0  /* no hardware forced shutdown */
#define MID_GPIO_NOACL			0  /* no kernel active low API */
#define MID_GPIO_FD			2  /* number of GPIOs fd to monitor */
#define GPIO_MOD_RESOUT2_INIT_ST	BOTH   /* both falling and rising edges */
#define GPIO_MOD_RESOUT2_POL		ACTIVE_HIGH
#define GPIO_MOD_ON_INIT_ST		INACTIVE
#define GPIO_MOD_ON_POL			ACTIVE_HIGH
#define GPIO_MOD_SERVICE_INIT_ST	INACTIVE
#define GPIO_MOD_SERVICE_POL		ACTIVE_LOW
#define GPIO_MOD_PWRRSTIN_INIT_ST	BOTH   /* both falling and rising edges */
#define GPIO_MOD_PWRRSTIN_POL		ACTIVE_HIGH
#define GPIO_MOD_PWRRSTOUT_INIT_ST	INACTIVE
#define GPIO_MOD_PWRRSTOUT_POL		ACTIVE_HIGH

#endif /* ifndef MID_SETTINGS_H */
