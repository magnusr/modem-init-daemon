/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_LOG_H
#define MID_LOG_H

#include <stdio.h>
#include "mid.h"

#ifdef DEBUG_TRACE
#define MID_LEVEL_MAX     5
extern struct mid_dbg mid_dbg_info;

#define MID_LEVEL_VERBOSE 5
#define MID_LEVEL_DEBUG   4
#define MID_LEVEL_INFO    3
#define MID_LEVEL_WARNING 2
#define MID_LEVEL_ERROR   1
#define MID_LEVEL_MIN     0

#ifndef HAVE_ANDROID_OS

#define MLGV(format, args...) do if (\
mid_dbg_info.mid_dbg_level > MID_LEVEL_DEBUG) {\
fprintf((mid_dbg_info.log_to_file && mid_dbg_info.log_fd) ? mid_dbg_info.log_fd : stdout, \
	"MID_VERBOSE " format " -- [%s, %d]\n", ##args, __FUNCTION__, __LINE__); \
fflush(mid_dbg_info.log_to_file ? mid_dbg_info.log_fd : stdout); } \
 while (0)

#define MLGD(format, args...) do if (\
mid_dbg_info.mid_dbg_level > MID_LEVEL_INFO) {\
fprintf((mid_dbg_info.log_to_file && mid_dbg_info.log_fd) ? mid_dbg_info.log_fd : stdout, \
	"MID_DEBUG " format " -- [%s, %d]\n", ##args, __FUNCTION__, __LINE__); \
fflush(mid_dbg_info.log_to_file ? mid_dbg_info.log_fd : stdout); } \
 while (0)

#define MLGI(format, args...) do if (\
mid_dbg_info.mid_dbg_level > MID_LEVEL_WARNING) {\
fprintf((mid_dbg_info.log_to_file && mid_dbg_info.log_fd) ? mid_dbg_info.log_fd : stdout, \
	"MID_INFO " format " -- [%s, %d]\n", ##args, __FUNCTION__, __LINE__); \
fflush(mid_dbg_info.log_to_file ? mid_dbg_info.log_fd : stdout); } \
 while (0)

#define MLGW(format, args...) do if (\
mid_dbg_info.mid_dbg_level > MID_LEVEL_ERROR) {\
fprintf((mid_dbg_info.log_to_file && mid_dbg_info.log_fd) ? mid_dbg_info.log_fd : stdout, \
	"MID_WARNING " format " -- [%s, %d]\n", ##args, __FUNCTION__, __LINE__); \
fflush(mid_dbg_info.log_to_file ? mid_dbg_info.log_fd : stdout); } \
 while (0)

#define MLGE(format, args...) do if (\
mid_dbg_info.mid_dbg_level > MID_LEVEL_MIN) {\
fprintf((mid_dbg_info.log_to_file && mid_dbg_info.log_fd) ? mid_dbg_info.log_fd : stdout, \
	"MID_ERROR " format " -- [%s, %d]\n", ##args, __FUNCTION__, __LINE__); \
fflush(mid_dbg_info.log_to_file ? mid_dbg_info.log_fd : stdout); } \
 while (0)

#else

#define LOG_TAG "MID"
#include <utils/Log.h>

#define MLGV(format, args...) do if (\
mid_dbg_info.mid_dbg_level > MID_LEVEL_DEBUG) {\
LOGV(format " -- [%s, %d]", ##args, __FUNCTION__, __LINE__); } \
 while(0)
#define MLGD(format, args...) do if (\
mid_dbg_info.mid_dbg_level > MID_LEVEL_INFO) {\
LOGD(format " -- [%s, %d]", ##args, __FUNCTION__, __LINE__); } \
 while(0)
#define MLGI(format, args...) do if (\
mid_dbg_info.mid_dbg_level > MID_LEVEL_WARNING) {\
LOGI(format " -- [%s, %d]", ##args, __FUNCTION__, __LINE__); } \
 while(0)
#define MLGW(format, args...) do if (\
mid_dbg_info.mid_dbg_level > MID_LEVEL_ERROR) {\
LOGW(format " -- [%s, %d]", ##args, __FUNCTION__, __LINE__); } \
 while(0)
#define MLGE(format, args...) do if (\
mid_dbg_info.mid_dbg_level > MID_LEVEL_MIN) {\
LOGE(format " -- [%s, %d]", ##args, __FUNCTION__, __LINE__); } \
 while(0)

#endif /* ifndef HAVE_ANDROID_OS */

#else

#define MLGV(format, args...) do {} while (0)
#define MLGD(format, args...) do {} while (0)
#define MLGI(format, args...) do {} while (0)
#define MLGW(format, args...) do {} while (0)
#define MLGE(format, args...) do {} while (0)

#endif /* ifdef DEBUG_TRACE */

#endif /* ifndef MID_LOG_H */
