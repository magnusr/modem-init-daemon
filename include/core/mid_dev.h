/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_DEV_H
#define MID_DEV_H

#include <stdint.h>

#include <apr_thread_proc.h>
#include <apr_pools.h>

#include "mid_mod_type.h"

/* U33x modem */
struct mf_type_U33x {
	enum mid_boot_prot_type boot_protocol;
	int skip_stat_data;
	char *stat_data;
	char *twosbc_acc;
	char *twosbc_app;
	char *emif_data;
	char *main_acc;
	char *main_app;
};

/* U5xx modem */
struct mf_type_U5xx {
	enum mid_boot_prot_type boot_protocol;
	char *secsbc;
	char *trdsbc;
	char *patch;
};

union mid_modem_firmware {
	enum mid_boot_prot_type boot_protocol;
	struct mf_type_U33x U33x;
	struct mf_type_U5xx U5xx;
};

struct mid_dev_internal;

struct mid_dev {
	/* device type*/
	enum mid_dev_type devtype;
	struct mid_dev_internal* internal;
	/* Common part */
	int fd;
	const char *name;
	apr_pool_t* pool;
	/* USB specific */
	char *id;
	struct flb_handle *flb;
	uint16_t vid;
	uint16_t pid;
	uint16_t index;
	struct flb_handle * (* flb_alloc_usb_unit)(const char *id, uint16_t vid, uint16_t pid, uint16_t index);
	/* HSI specific */
	int disable_config;
	int flow;
	int frame_size;
	int channels;
	int divisor;
	int mode;
	int arb_mode;
};

int mid_dev_open(struct mid_dev *dev);
int mid_dev_close(struct mid_dev *dev);
int mid_configure_dev(struct mid_dev *dev, enum mid_dev_type);

#endif /* ifndef MID_DEV_H */
