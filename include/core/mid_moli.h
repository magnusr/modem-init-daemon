/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_MOLI_H
#define MID_MOLI_H

#include <unistd.h>

#include <apr_thread_proc.h>

#include "mid_dev.h"
#include "crash_dump.h"
#include "mid.h"

/* Configuration file processing */
void moli_check_cfgfile(struct mid *mid_info);

/* Default settings for device and modem firmware */
void moli_default_settings(struct mid *mid_info);

/* Modem boot function */
void * moli_boot(apr_thread_t* thread, void *arg);

/* Security handling */
int moli_configure_security_data(struct mid *mid_info);

/* Crash dump */
int moli_get_crash_dump(struct mid *midinfo, struct mid_dev *dev, const char* dump_directory, const char* dump_directory_header, const char * dump_filename, const char* dump_filename_header, int dump_timestamp, crash_dump_report_t report_type, int max_dump_files);

int moli_fw_upgrade_flash();
void moli_fw_upgrade_init(int *error_code_p);

/**
 * Checks the status of the modem fw upgrade state machine.
 *
 * Returns a status value as indicated below:
 *
 * 0 - Not installed
 * 1 - Initiating
 * 2 - Initiated
 * 3 - Applying
 * 4 - Applied
 * 5 - Pending finalization
 * 6 - Installed
 * 7 - Pending rollback
 * 8 - Pending cleanup
 * 9 - Restart
 *
 * -1 if error
 */
int moli_fw_upgrade_status(char **status);
int moli_fw_upgrade_img_xfer(struct mid *mid_info);

char * moli_fw_upgr_modem_error_code_str();

#endif /* ifndef MID_MOLI_H */
