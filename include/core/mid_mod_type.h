/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_MOD_TYPE_H
#define MID_MOD_TYPE_H

enum mid_boot_prot_type {
	U33x = 0x00,
	U5xx = 0x01
};

enum mid_dev_type {
	MID_DEV_SERIAL_96	= 0x00,
	MID_DEV_SERIAL_115	= 0x01,
	MID_DEV_SERIAL_460	= 0x02,
	MID_DEV_SERIAL_921	= 0x03,
	MID_DEV_SPI_13_1024	= 0x04,
	MID_DEV_USB		= 0x05,
	MID_DEV_HSI		= 0x06
};

#endif /* ifndef MID_MOD_TYPE_H */
