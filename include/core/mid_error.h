/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_ERROR_H
#define MID_ERROR_H

#define EMIDBPNOMSG		1	/* Boot protocol no received message */
#define EMIDBPTIMEOUT		2	/* Boot protocol timeout */
#define EMIDBPINVMSG		3	/* Boot protocol invalid message */
#define EMIDBPTXMSG		4	/* Boot protocol error on TX message */
#define EMIDBP			5	/* Boot protocol error */
#define EMIDIPCCON		6	/* IPC medium connect error */
#define EMIDIPCDIS		7	/* IPC medium disconnect error */
#define EMIDIPCINVMSG		8	/* IPC invalid message */
#define EMIDIPCTXMSG		9	/* IPC error on TX message */
#define EMIDGPIO		10	/* MID GPIO error */
#define EMIDFS			11	/* MID filesystem error */
#define EMID			12	/* MID internal error */
#define EMIDAT			13	/* AT channel error */

#endif /* ifndef MID_ERROR_H */
