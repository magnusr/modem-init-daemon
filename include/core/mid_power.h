/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_POWER_H
#define MID_POWER_H

#include <stdbool.h>
#include "mid.h"

struct mid_power;

struct mid_power* mid_power_init(void);

bool mid_permit_suspend(struct mid_power* context);
bool mid_prevent_suspend(struct mid_power* context);
void mid_enable_io_safe_mode(struct mid *midinfo);
void mid_disable_io_safe_mode(struct mid *midinfo);
void mid_enable_gpio_service_safe_mode(struct mid *midinfo);
void mid_disable_gpio_service_safe_mode(struct mid *midinfo);
void mid_enable_gpio_out_safe_mode(struct mid *midinfo);
void mid_disable_gpio_out_safe_mode(struct mid *midinfo);
void mid_enable_tx_safe_mode(struct mid *midinfo);
void mid_disable_tx_safe_mode(struct mid *midinfo);
void mid_platform_reboot(struct mid *midinfo) __attribute__ ((noreturn));

#ifdef HAVE_ANDROID_OS
int mid_set_wakelock(struct mid_power* context, char *name, int set);
#endif /* HAVE_ANDROID_OS */

#endif /* ifndef MID_POWER_H */
