/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_H
#define MID_H

#include <stdio.h>

#define UNUSED(expr) do { (void)(expr); } while (0)
#define likely(val)	__builtin_expect((val), 1)
#define unlikely(val)	__builtin_expect((val), 0)

#define MID_FLBOOT_FAILED -1
#define MID_FLBOOT_OK      0


#define SET_FLAG(flags, bit)	(flags |=  (1<<bit))
#define CLEAR_FLAG(flags, bit)	(flags &= ~(1<<bit))
#define FLAG_UP(flags, bit)	(flags & (1<<bit))

struct mid;

#ifdef DEBUG_TRACE
struct mid_dbg {
/* LOG settings */
	char *logfile;
	FILE *log_fd;
	int log_to_file;
	int mid_dbg_level;
};
#endif

#if defined(MOTOROLA_FEATURE)
struct mot_mid {
        const char *itp_mode_cmd;
        const char *norm_mode_cmd;
        const char *svc_mode_cmd;
        const char *recovery_mod_prefl_cmd;
	int recovery_mode;
        int dual_boot_mode;
};
#endif

#if defined(MOTOROLA_FEATURE)
extern struct mot_mid mot_mid_info;
#endif

#endif /* ifndef MID_H */
