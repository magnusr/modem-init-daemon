/*
 * Copyright (C) 2011 ST-Ericsson AS
 * Author: Roger Froysaa / roger.xx.froysaa@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_FW_UPGRADE__H
#define MID_FW_UPGRADE__H


#define MID_FW_XFER_FAILED -1
#define MID_FW_XFER_OK      0
#define MID_FW_UPGR_OK      0


struct fw_upgr_method {
	char * string;
	int req_type;
};

typedef struct mid_fw_upgr_error_code_s
{
	char * str;
	int error_code;
} mid_fw_upgr_error_code_t;

typedef enum fw_upgrade_req_e {
	MID_FW_UPGR_REQ_NONE = 0,
	MID_FW_UPGR_REQ_IMG_XFER,
	MID_FW_UPGR_REQ_FLASH,
	MID_FW_UPGR_REQ_STATUS
} fw_upgrade_req_t;

static const char * const MID_fw_upgrade_req_name[] =
{
	"MID_FW_UPGR_REQ_NONE",
	"MID_FW_UPGR_REQ_IMG_XFER",
	"MID_FW_UPGR_REQ_FLASH",
	"MID_FW_UPGR_REQ_STATUS"
};

typedef enum fw_upgrade_result_e {
	MID_FW_UPGR_RSLT_SYSTEM_ERROR = 100,
	MID_FW_UPGR_RSLT_INVALID_REQ_PHASE,
	MID_FW_UPGR_RSLT_REQ_IN_PROGRESS,
	MID_FW_UPGR_RSLT_UNSUPP_MODEM,
	MID_FW_UPGR_RSLT_MODEM_SHUTDOWN,
	MID_FW_UPGR_RSLT_AT_TIMEOUT,
	MID_FW_UPGR_RSLT_REJECTED,
	MID_FW_UPGR_RSLT_AT_CHN_ERROR,
	MID_FW_UPGR_RSLT_AT_CMD_ERROR,
	MID_FW_UPGR_RSLT_INVALID_MODEM_STATE,
	MID_FW_UPGR_RSLT_FS_ERROR,
	MID_FW_UPGR_RSLT_IMG_SIZE_ERROR,
	MID_FW_UPGR_RSLT_LOW_BATT,
	MID_FW_UPGR_RSLT_NO_FW_IMG,
} fw_upgrade_result_t;

#endif // MID_FW_UPGRADE__H
