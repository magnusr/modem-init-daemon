/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_CONF_H
#define MID_CONF_H

#include <stdbool.h>

#include <libconfig.h>

#include <mid_gpio.h>

int my_lookup_int(const config_t *config, char* name, int* value, char* runtime_mode);
int my_lookup_bool(const config_t *config, char* name, int* value, char* runtime_mode);
int my_lookup_string(const config_t *config, char* name, const char** value, char* runtime_mode);
enum modem_arch_id {
	UNKNOWN = 0x0,
	OSMIUM,
	HASSIUM,
	THORIUM
};

typedef enum {
	SHUTDOWN_MODE_NORMAL               = 0,
	SHUTDOWN_MODE_RESOUT2_MODEM_IS_OFF = 1,
} modem_shutdown_mode_t;

struct mid_config {
	char *runtime_mode;
	char *modem_arch;
	enum modem_arch_id modem_arch_id;
	int initial_enable_bootldr;
	char *initial_rdir;
	int initial_sec_data_conf;
	char *sec_data_file;
	struct mid_dev *prim_dev_config;
	struct mid_dev *sec_dev_config;
	union mid_modem_firmware *mod_firm_config;
	int daemonize;
	char * fw_upgr_img_dir;
	char * fw_upgr_file_name;
	char *pidfile;
	int link_switch;
	/* IPC settings */
	int boot_retry;
	int flash_retry;
	int shutdown_retry;
	int flash_force_pwroff;
	int gpio_ctrl;
	char* gpio_base_path;
	int gpio_no_xport;
	int gpio_no_dir;
	int gpio_no_aclow;
	struct gpio mid_gpios[GPIO_ID_COUNT];
	/* Command strings */
	const char *mod_ifup_cmd;
	const char *mod_on_cmd;
	const char *mod_off_cmd;
	const char *mod_init_cmd;
	const char *mod_prefl_cmd;
	const char *mod_postfl_cmd;
	const char *mod_pre_crashdump_cmd;
	const char *mod_post_crashdump_cmd;
	const char *mfa_cmd;
	/* */
	int rfm_delay;
	/* AT command availablility flags */
	int enable_at_channel;
	int enable_low_battery_subscription;
	int enable_high_temperature_subscription;
	int enable_efwd_atcfun;
	int enable_atcgmr;
	/* */
	const char *dump_filename;
	const char *dump_filename_header;
	const char *dump_directory;
	const char *dump_directory_header;
	const char *silent_panic_detection_file;
	const char *critical_panic_detection_file;
	int max_dump_files;
	int dump_timestamp;
	int crash_dump_type;
	int crashdump_dev_mode;
	int postsec_reboot;
	int use_on_sw_c;
	int use_atcfun0;
	int force_gpio_pwroff;
	modem_shutdown_mode_t modem_shutdown_mode;
	int use_cpu_reset_reg;
	const char *cpu_reset_reason;
	int reboot_on_failure;
	unsigned long *safe_ioval;
	size_t nb_elem_sioval;
	unsigned long *normal_ioval;
	size_t nb_elem_nioval;
	/* Debug settings */
	bool dbus_disable_modem_reboot;
	bool dbus_dump_on_reboot;

	int timeout_flashing;

	const char* mfa_service_name; // Name of Android MFA service
	const char* mfa_start_command; // Command line for non-Android Linux
	const char* mfa_stop_command; // Command line for non-Android Linux
	const char* mfa_control_path; // Path to MFA control Unix Domain Socket
};

bool mid_conf_load(struct mid_config* config, const char* cfg_file);

#endif /* ifndef MID_CONF_H */
