/*
 * Copyright (C) 2011 ST-Ericsson AS
 * Author: Magnus Reftel / magnus.xm.reftel@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef INCLUDE_GUARD_MID_MFA_H
#define INCLUDE_GUARD_MID_MFA_H

#include <stdbool.h>

#include <apr_pools.h>
#include <apr_poll.h>

struct mid;
struct mid_mfa;
struct mid_config;

struct mid_mfa* mid_mfa_init(struct mid* mid_info, const struct mid_config* config, apr_pool_t* pool);
void mid_mfa_destroy(struct mid_mfa*);

bool mid_mfa_start(struct mid_mfa*);
void mid_mfa_stop(struct mid_mfa*, bool success);

bool mid_mfa_notify_on(struct mid_mfa*);
bool mid_mfa_was_successful(struct mid_mfa* context);

void mid_mfa_receive_message(struct apr_pollfd_t*, struct mid*, struct mid_mfa*);
bool mid_mfa_get_fd(struct mid_mfa*, apr_pollfd_t* pollfd);

#endif
