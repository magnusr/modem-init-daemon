/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Erwan Bracq / erwan.bracq@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_FLASH_H
#define MID_FLASH_H

#include "mid.h"

#define FILE_NAME_SZ 512
#define DIR_NAME_SZ FILE_NAME_SZ * 2

int cleanupdirname(struct mid *mid_info);
int isdirempty(const char* dirname);
int purgedir(const char* dirname);

#endif /* ifndef MID_FLASH_H */
