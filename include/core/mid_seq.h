/*
 * Copyright (C) 2010 ST-Ericsson AS
 * Author: Dmitry Tarnyagin / dmitry.tarnyagin@stericsson.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */

#ifndef MID_SEQ_H_INCLUDED
#define MID_SEQ_H_INCLUDED

#include <sys/types.h>

/* Sequence of characters inside a string */
struct seq_t {
	const char *seq; /* First character part of the sequence */
	const char *end; /* First character after the end of the sequence */
};

/* Returns pointer to the first character in the sequence */
static inline const char *seq_str(const struct seq_t *seq)
{
	return seq->seq;
}

/* Returns length of the sequence */
static inline size_t seq_len(const struct seq_t *seq)
{
	if (seq->end > seq->seq)
		return seq->end - seq->seq;
	else
		return 0;
}

/* Checks if the sequence is empty */
static inline int seq_empty(const struct seq_t *seq)
{
	return seq->seq >= seq->end;
}

/* Removes left whitespaces from the sequence */
void seq_triml(struct seq_t *seq);
/* Removes roght whitespaces from the sequence */
void seq_trimr(struct seq_t *seq);
/* Separates sequence l by two parts l and r using set of delimiters tok */
void seq_tok(struct seq_t *l, struct seq_t *r,
	     const char *tok);
/* Copies sequence to an external buffer */
int seq_copyto(const struct seq_t *seq, char *buf, size_t buf_size);

#endif
